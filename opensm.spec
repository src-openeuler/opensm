Name:             opensm
Version:          3.3.24
Release:          2
Summary:          InfiniBand Subnet Manager and Administrator
License:          GPL-2.0-only OR Linux-OpenIB
URL:              https://github.com/linux-rdma/opensm

Source0:          https://github.com/linux-rdma/opensm/releases/download/%{version}/%{name}-%{version}.tar.gz
Source1:          opensm.logrotate
Source2:          opensm.service
Source3:          opensm.launch
Source4:          opensm.rwtab
BuildRequires:    gcc byacc flex libibumad-devel autoconf automake libtool

Requires:         logrotate rdma
ExcludeArch:      %{arm}
%{?systemd_requires}

Provides:         opensm-libs = %{version}-%{release}
Provides:         opensm-libs%{?_isa} = %{version}-%{release}
Obsoletes:        opensm-libs < %{version}-%{release}

%description
OpenSM provides an implementation for an InfiniBand Subnet Manager and
Administrator. Such a software entity is required to run for in order
to initialize the InfiniBand hardware (at least one per each
InfiniBand subnet).

The full list of OpenSM features is described in the user manual
provided in the doc sub directory.

%package devel
Summary:          Development files for opensm
Requires:         %{name} = %{version}-%{release}
Provides:         opensm-static = %{version}-%{release}
Provides:         opensm-static%{?_isa} = %{version}-%{release}
Obsoletes:        opensm-static < %{version}-%{release}

%description devel
This package contains libraries for developing applications that use
opensm and static version of opensm libraries

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh
%configure --with-opensm-conf-sub-dir=rdma \
           CFLAGS="$CFLAGS -fno-strict-aliasing"

%make_build

cd opensm
./opensm -c ../opensm-%{version}.conf

%install
%make_install

%delete_la

rm -fr %{buildroot}%{_sysconfdir}/init.d
install -D -m644 opensm-%{version}.conf %{buildroot}%{_sysconfdir}/rdma/opensm.conf
install -D -m644 %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/opensm
install -D -m644 %{SOURCE2} %{buildroot}%{_unitdir}/opensm.service
install -D -m755 %{SOURCE3} %{buildroot}%{_libexecdir}/opensm-launch
install -D -m644 %{SOURCE4} %{buildroot}%{_sysconfdir}/rwtab.d/opensm
mkdir -p ${RPM_BUILD_ROOT}/var/cache/opensm

%preun
%systemd_preun opensm.service

%post
%systemd_post opensm.service

%postun
rm -rf /var/cache/opensm

%systemd_postun_with_restart opensm.service

%files
%license COPYING
%doc AUTHORS
%dir /var/cache/opensm
%config(noreplace) %{_sysconfdir}/logrotate.d/opensm
%config(noreplace) %{_sysconfdir}/rdma/opensm.conf
%{_sysconfdir}/rwtab.d/opensm
%{_unitdir}/*
%{_libdir}/*.so.*
%{_libexecdir}/opensm-launch
%{_sbindir}/opensm
%{_sbindir}/osmtest

%files devel
%{_libdir}/lib*.so
%{_includedir}/infiniband
%{_libdir}/lib*.a

%files help
%doc ChangeLog INSTALL README NEWS
%{_mandir}/man5/*5*
%{_mandir}/man8/*8*

%changelog
* Thu Dec 26 2024 Funda Wang <fundawang@yeah.net> - 3.3.24-2
- cleanup spec

* Wed May 18 2022 wulei <wulei80@h-partners.com> - 3.3.24-1
- Update package

* Web 02 Jun 2021 zhaoyao<zhaoyao32@huawei.com> - 3.3.20-12
- fixs faileds: /bin/sh: gcc: command not found. 

* Mon Nov 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.3.20-10
- Package init
